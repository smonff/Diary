##
# Diary
#
# @file
# @version 0.1

DATA_DIR=/tmp/diary

test:
	carton exec -- yath  -MDDP -v test t/

entry:
	carton exec perl ./script/diary --data-dir $(DATA_DIR) --project-name diary

build-readme:
	carton exec -- pod2readme --format 'markdown' lib/App/Standup/Diary.pm README.md

mb-test-release:
	carton exec mbtiny test --release

mb-regenerate:
	carton exec mbtiny regenerate

mb-bump:
	carton exec mbtiny regenerate --bump


mb-dist:
	carton exec mbtiny dist

upload:
	carton exec mbtiny upload

clean:
	rm -rf ./.build
	rm -f ./MYMETA.* ./README ./README.md.bak



# end
