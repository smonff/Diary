# NAME

App::Standup::Diary - Manage a simple Markdown journal for your daily standups

# SYNOPSIS

    # From command line
    diary --data-dir /home/smonff/diary/ --project-name SeriousWork

    # In your Perl code
    my $diary = App::Standup::Diary
      ->new(
        data_dir     => $data_dir,
        project_name => $project_name
    );

    $diary->write();

# DESCRIPTION

This module is the implementation of the [diary](https://metacpan.org/pod/diary) command, that can help to keep
a directory of organized daily notes aimed at standup preparation and
presentation.

It provides a couple of ways to customize the notes template.

It use internal tools built with CPAN modules:

- [App::Standup::Diary::Template](https://metacpan.org/pod/App%3A%3AStandup%3A%3ADiary%3A%3ATemplate)

    The Markdown template where your data are interpolated.

- [Standup::Role::Date](https://metacpan.org/pod/Standup%3A%3ARole%3A%3ADate)

    Provide date features for all the `diary` uses.

- [Standup::Role::Project](https://metacpan.org/pod/Standup%3A%3ARole%3A%3AProject)

    Provide a project name, so far.

# MOTIVATIONS

Daily standups are a common, tried and tested modern work methodology. They are
_"brief, daily collaboration meeting in which the team review progress from the
previous day, declares intentions for the current day, and highlights any
obstacles encountered or anticipated"_ ([source](https://metacpan.org/pod/brief%2C%20daily%20collaboration%0Ameeting%20in%20which%20the%20team%20review%20progress%20from%20the%20previous%20day%2C%20declares%0Aintentions%20for%20the%20current%20day%2C%20and%20highlights%20any%20obstacles%20encountered%20or%0Aanticipated.)).

This tool is supposed to provide self-support for persons:

- Who struggle with daily standups presentations oral expression
- Surely familiar with the Perl ecosystem

## How did it start?

Social anxiety can make my standup presentation very confusing. I also tend to
forget key points if improvising, due to the stress of having no talk notes
support. Keeping a diary of my thoughts drastically helped me to stay calm and
collaborate better with the various teams I worked with. And if they are well
sorted by year, month and day, it makes very easy to find old notes for eventual
later usage.

## Ready for production

I have been using it at work since 2021 and it helped me to reduce the stress of
standups and meeting.

## Methodology

Every morning, create the daily file by running `diary`. It's template is a
simple 3 items list. Open the day file in `$data_dir/$year/$month/` and stash
your thoughts by using the following methodology:

- `done`

    List of tasks you accomplished in the previous day

- `todo`

    List of tasks you plan to accomplish today

- `blockers`

    List of eventual blockers, so that your colleagues can support you

Then just read the notes during the daily standup.

## Experiment with Object::Pad

[App::Standup::Diary](https://metacpan.org/pod/App%3A%3AStandup%3A%3ADiary) is my pet project for Perl's Corinna implementation of the
core OOP features. `diary` use [Object::Pad](https://metacpan.org/pod/Object%3A%3APad), not the `class` feature
introduced in Perl `5.40`. [Object::Pad](https://metacpan.org/pod/Object%3A%3APad) is the test bed for the new core OO
system.

# INSTALLATION

## For common usage

    cpanm App::Standup::Diary

It will make the `diary` command available in the `~/perl5/bin/` directory.
Should be available in your `PATH`.

See [diary](https://metacpan.org/pod/diary) for command line usage.

## For development

    # Install a Perl module manager
    apt install carton

    git clone git@codeberg.org:smonff/Diary.git

    cd Diary

    # Install CPAN dependencies
    carton install

# How to use it?

I set an alias in my `.bashrc`. Should also work in your own-favorite-shell:

    alias diary="diary --data-dir /home/smonff/diary --project-name SeriousWork";

Each morning, before my work standup, I run `diary`. It create a Markdown file
in the specified `--project-name` directory. I then edit my thoughts with an
editor.

See [diary](https://metacpan.org/pod/diary) for command line usage.

# FIELDS

## config

## daily\_data\_path

## data\_dir

## template

A [App::Standup::Diary::Template](https://metacpan.org/pod/App%3A%3AStandup%3A%3ADiary%3A%3ATemplate) object.

# METHODS

## build\_full\_file\_path()

## build\_path($self->date->ymd('/'))

Use the date from `Standup::Role::Date` to build the final path file name.

## create\_directories\_tree()

If `$self-`should\_create\_dir()> returns a true value, it would take care of the
directory creation using [Path::Tiny](https://metacpan.org/pod/Path%3A%3ATiny) `mkpath`.

## init\_daily\_data\_path($file\_path)

Helper that initialize `$daily_data_path` with a `Path::Tiny` instance
for the current day diary entry.

    # foo/2022/03 (Path::Tiny)
    $self->init_daily_data_path($self->build_path)

## should\_create\_dir()

Check if a new _year_ or _month_ directory should be created so that we can
store the standup file. In simpler words: are we the first day of the year, or
of the month?

## write()

This is `App::Standup::Diary` entry point, AKA `main()`.

# TODOs

- Make the template configurable by setting it in a separate file
- Retrieve TODOS from previous days

See the [issues tracker](https://codeberg.org/smonff/Standup-Diary/issues).

# SEE ALSO

A couple of similar tools:

- [StandupGenerator](https://metacpan.org/release/JTREEVES/StandupGenerator-0.5/source/README.md)

    On the CPAN, this is pretty much all what I found. I like the spirit of this
    one.

- [Almanac](https://codeberg.org/jameschip/almanac)

    A similar effort written in Bash.

# ACKNOWLEDGEMENTS

Thanks to the maintainers of [Config::Tiny](https://metacpan.org/pod/Config%3A%3ATiny), [Mojo::Template](https://metacpan.org/pod/Mojo%3A%3ATemplate), [Object::Pad](https://metacpan.org/pod/Object%3A%3APad),
[Path::Tiny](https://metacpan.org/pod/Path%3A%3ATiny), [Time::Piece](https://metacpan.org/pod/Time%3A%3APiece).

# LICENSE

Copyright 2022-2024 Sebastien Feug�re

This library is free software; you can redistribute it and/or modify it under
the Artistic License 2.0.

See [perlartistic](https://metacpan.org/pod/perlartistic).

# AUTHOR

S�bastien Feug�re - seb@feugere.net
